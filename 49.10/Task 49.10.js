$(document).ready(function() {
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    var gOrderObject = [];
    var gNewOrderObject = [];
    var gOrderObjectId = [];
    var gOrderID = "";
    
    var gDataRow = "";
    const gCOLUMN_ORDER_ID = 0;
    const gCOLUMN_KICH_CO_COMBO = 1;
    const gCOLUMN_LOAI_PIZZA = 2;
    const gCOLUMN_NUOC_UONG = 3;
    const gCOLUMN_THANH_TIEN = 4;
    const gCOLUMN_HO_VA_TEN = 5;
    const gCOLUMN_SO_DIEN_THOAI = 6;
    const gCOLUMN_TRANG_THAI = 7;
    const gACTION_COL = 8;
    var gPizzaData = [
        { "kichCo": "S", "duongKinh": 20, "suon": 2, "salad": 200, "soLuongNuoc": 2, "thanhTien": 150000 },
        { "kichCo": "M", "duongKinh": 25, "suon": 4, "salad": 300, "soLuongNuoc": 3, "thanhTien": 200000 },
        { "kichCo": "L", "duongKinh": 30, "suon": 8, "salad": 500, "soLuongNuoc": 4, "thanhTien": 250000 },
    ];

    const gCOLUMN_DATA = ["orderId", "kichCo", "loaiPizza", "idLoaiNuocUong", "thanhTien", "hoTen", "soDienThoai", "trangThai", "action"];
    // định nghĩa table  - chưa có data
    var gOrderTable = $("#order-table").DataTable({
        // Khai báo các cột của datatable
        "columns": [
            { "data": gCOLUMN_DATA[gCOLUMN_ORDER_ID] },
            { "data": gCOLUMN_DATA[gCOLUMN_KICH_CO_COMBO] },
            { "data": gCOLUMN_DATA[gCOLUMN_LOAI_PIZZA] },
            { "data": gCOLUMN_DATA[gCOLUMN_NUOC_UONG] },
            { "data": gCOLUMN_DATA[gCOLUMN_THANH_TIEN] },
            { "data": gCOLUMN_DATA[gCOLUMN_HO_VA_TEN] },
            { "data": gCOLUMN_DATA[gCOLUMN_SO_DIEN_THOAI] },
            { "data": gCOLUMN_DATA[gCOLUMN_TRANG_THAI] },
            { "data": gCOLUMN_DATA[gACTION_COL] }
        ],
        // Ghi đè nội dung của cột action, chuyển thành button chi tiết
        "columnDefs": [{
            targets: gACTION_COL,
            className: "text-center",
            defaultContent: `
        <button class="btn btn-link" data-toggle="tooltip" data-placement="bottom" title="Edit" id = "btn-edit"><i class="fas fa-edit" text-success style = "cursor:pointer; color: green"></i></button>
        <button class="btn btn-link" data-toggle="tooltip" data-placement="bottom" title="Delete" id = "btn-delete"><i class="fas fa-trash-alt" text-success style = "cursor:pointer; color: red"></i></button>
        `
        }]
    });
    /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
    onPageLoading();
    // getDrinkList();
    //loadPizzaSizeToModal(gPizzaSize);
    $("#btn-filter-order").on("click", function() {
        onBtnFilterOrderClick();
    })

    $("#btn-add-order").on("click", function() {
        onBtnAddNewOrderClick();
    })
    
    $("#input-create-kich-co").on("change", function() {
        onBtnChangeKichCoSelect();
    })

    $("#btn-insert-order").on("click", function() {
        onBtnInsertNewOrderClick();
    })

    $("#order-table").on("click", "#btn-edit", function() {
        onBtnEditOrderClick(this);
    })

    $("#btn-modal-update-order").on("click", function(){
        onBtnModalUpdateOrderClick();
    })

    $("#order-table").on("click", "#btn-delete", function() {
        onBtnDeleteClick(this);
    })

    $("#btn-modal-confirm-delete-order").on("click", function() {
        onBtnModalDeleteConfirmOrderClick();
    });

   
    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    // hàm chạy khi trang được load
    function onPageLoading() {
        // lấy data từ server
        callApiToGetAllOrder();
        loadDataToTable(gOrderObject);
    }

    function onBtnEditOrderClick(paramButtonElement) {
        "use strict";
        console.log("Nút Sửa được nhấn!");
        var vRowClick = $(paramButtonElement).closest("tr"); // xác định tr chứa nút bấm được click
        var vTable = $("#order-table").DataTable(); // tạo biến truy xuất đến DataTable
        var vDataRow = vTable.row(vRowClick).data(); // Dữ liệu của hàng dữ liệu chứa nút bấm được click
        gDataRow = vDataRow;
        gOrderID = vDataRow.orderId;
        //console.log(gOrderID);

        // call Api lấy dữ liệu đơn hàng qua OrderId
        callApiToGetDataByOrderID(gOrderID);
        // gán dữ liệu vào modal sửa đơn hàng
        loadDataToModal(gOrderObjectId);
        // hiện modal trắng sửa đơn hàng lên
        $("#edit-order-modal").modal("show");
    }

    function callApiToGetDataByOrderID(gOrderID){
        "use strict";
        const gBASE_URL = "http://42.115.221.44:8080/devcamp-pizza365/orders";
        $.ajax({
            url: gBASE_URL + "/" + gOrderID,
            async: false,
            type: "GET",
            dataType: 'json',
            success: function(responseObject){
                //debugger;
                console.log(responseObject);
                gOrderObjectId = responseObject;
            },
            error: (error) => {
                console.assert(error.responseText);
            }
        })
    }

    function onBtnFilterOrderClick() {
        //B1. Khai báo biên và thu thập dữ liệu
        var vFilterObj = {
            trangThai: "",
            loaiPizza: ""
        }
        getFilterData(vFilterObj);
        //B2. Kiểm tra dữ liệu (bỏ qua)
        //B3. Lọc dữ liệu và hiển thị kết quả
        var filteredOrder = filterOrder(vFilterObj);
        loadDataToTable(filteredOrder);
    }

    function onBtnAddNewOrderClick() {
        "use strict";
        $("#insert-order-modal").modal("show");
        var vDrinkList = getDrinkList();
        loadDrinkToModal(vDrinkList);
    }

    function onBtnInsertNewOrderClick() {
        "use strict";
        //khai báo đối tượng chứa user data
        var vOrderObj = {
            kichCo: "",
            duongKinh: 0,
            suon: 0,
            salad: 0,
            loaiPizza: "",
            idVourcher: "",
            idLoaiNuocUong: "",
            soLuongNuoc: 0,
            hoTen: "",
            thanhTien: 0,
            email: "",
            soDienThoai: "",
            diaChi: "",
            loiNhan: ""
        }
        getOrderDataCreate(vOrderObj);
       
        //B2: Validate Insert
        var vIsValidate = validateData(vOrderObj);
        if (vIsValidate == true) {
            //console.log(vOrderObj);
            // B3: Insert user
            //gọi API lấy danh sách user
            callApiToCreateOrder(vOrderObj);

            //load dữ liệu vào bảng
            loadDataToTable(gOrderObject);
            // xóa trắng dữ liệu trên modal
            resertCreateOrderForm();
            // ẩn modal form
            $("#insert-order-modal").modal("hide");
        }
    }

    function onBtnDeleteClick(paramDelete) {
        "use strict";
        console.log('Nút delete được ấn !!!')
        var vCurrentRow = $(paramDelete).closest('tr');
        var vTable = $('#order-table').DataTable();
        gDataRow = vTable.row(vCurrentRow).data();
        console.log(gDataRow)
        $('#delete-confirm-modal').modal('show');
    }

    function onBtnModalUpdateOrderClick(){
        "use strict";
        gDataRow.trangThai = $("#select-update-trang-thai").val();
        console.log("nút update được ấn")
        // B1: đọc dữ liệu, mấy thông tin khác của đơn hàng không được thay đổi, trừ trạng thái đơn hàng
        var vUpdateOrder = {
            trangThai: gDataRow.trangThai //3 trang thai open, confirmed, cancel
        }
        console.log(gDataRow);
   
        // B2: Validate update (không có)
        // B3: update order
        callApiToUpdateOrder(gDataRow.id, vUpdateOrder);
        //gọi API lấy danh sách user
        callApiToGetAllOrder();
        //load dữ liệu vào bảng
        loadDataToTable(gOrderObject);
        // xóa trắng dữ liệu trên modal
        resertCreateOrderForm();
        // ẩn modal form
        $("#edit-order-modal").modal("hide");
    }

   
    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình */
    function callApiToGetAllOrder() {
        "use strict";
        $.ajax({
            url: "http://42.115.221.44:8080/devcamp-pizza365/orders",
            async: false,
            type: "GET",
            dataType: 'json',
            success: function(responseObject) {
                //debugger;
                gOrderObject = responseObject;
                console.log(gOrderObject);
            },
            error: function(error) {
                console.assert(error.responseText);
            }
        });
    }

    function filterOrder(paramFilter) {
        var result = [];
        //lọc dữ liệu trên user
        result = gOrderObject.filter(function(order) {
            return ((paramFilter.trangThai == "" || order.trangThai.toLowerCase() == paramFilter.trangThai.toLowerCase()) &&
                (paramFilter.loaiPizza == "" || order.loaiPizza.toLowerCase() == paramFilter.loaiPizza.toLowerCase()))
        })
        return result;
    }

    function getFilterData(paramFilterObj) {
        paramFilterObj.trangThai = $("#select-order-status option:selected").val();
        paramFilterObj.loaiPizza = $("#select-pizza-type option:selected").val();
    }

    function onBtnChangeKichCoSelect() {
        "use strict";
        var vKichCo = $("#input-create-kich-co").val();
        console.log(vKichCo);
        var vDataResult = getDataByKichCo(vKichCo);
        console.log(vDataResult);
        $('#input-create-duong-kinh').empty();
        $('#input-create-suon').empty();
        $('#input-create-salad').empty();
        $('#input-create-so-luong-nuoc').empty();
        $('#input-create-thanh-tien').empty();
        if (vDataResult.length > 0) {
            $('#input-create-duong-kinh').removeAttr("disabled"); // Gỡ bỏ thuộc tính disabled
            $('#input-create-duong-kinh').val(vDataResult[0]);
            $('#input-create-duong-kinh').attr("disabled", "disabled");
            $('#input-create-suon').removeAttr("disabled"); // Gỡ bỏ thuộc tính disabled
            $('#input-create-suon').val(vDataResult[1]);
            $('#input-create-suon').attr("disabled", "disabled");
            $('#input-create-salad').removeAttr("disabled"); // Gỡ bỏ thuộc tính disabled
            $('#input-create-salad').val(vDataResult[2]);
            $('#input-create-salad').attr("disabled", "disabled");
            $('#input-create-so-luong-nuoc').removeAttr("disabled"); // Gỡ bỏ thuộc tính disabled
            $('#input-create-so-luong-nuoc').val(vDataResult[3]);
            $('#input-create-so-luong-nuoc').attr("disabled", "disabled");
            $('#input-create-thanh-tien').removeAttr("disabled"); // Gỡ bỏ thuộc tính disabled
            $('#input-create-thanh-tien').val(vDataResult[4]);
            $('#input-create-thanh-tien').attr("disabled", "disabled");

        } else {
            $('#input-create-duong-kinh').attr("disabled", "disabled"); // thêm thuộc tính disabled khi không có type nào
            $('#input-create-suon').attr("disabled", "disabled"); // thêm thuộc tính disabled khi không có type nào
            $('#input-create-salad').attr("disabled", "disabled"); // thêm thuộc tính disabled khi không có type nào
            $('#input-create-so-luong-nuoc').attr("disabled", "disabled"); // thêm thuộc tính disabled khi không có type nào
            $('#input-create-thanh-tien').attr("disabled", "disabled"); // thêm thuộc tính disabled khi không có type nào
        }
    }

    function getDataByKichCo(paramKichCo) {
        var vKichCo = false;
        var vDataResult = [
            { duongKinh: 0 },
            { suon: 0 },
            { salad: 0 },
            { soLuongNuoc: 0 },
            { thanhTien: 0 }
        ]
        var vIterator = 0;
        while (!vKichCo && vIterator < gPizzaData.length) {
            if (gPizzaData[vIterator].kichCo === paramKichCo) {
                vKichCo = true;
                vDataResult[0] = gPizzaData[vIterator].duongKinh;
                vDataResult[1] = gPizzaData[vIterator].suon;
                vDataResult[2] = gPizzaData[vIterator].salad;
                vDataResult[3] = gPizzaData[vIterator].soLuongNuoc;
                vDataResult[4] = gPizzaData[vIterator].thanhTien;
            } else {
                vIterator++;
            }
        }
        return vDataResult;
    }

    function getOrderDataCreate(paramOrderObj) {
        "use strict";
        paramOrderObj.kichCo = $("#input-create-kich-co").val();
        paramOrderObj.duongKinh = $("#input-create-duong-kinh").val();
        paramOrderObj.suon = $("#input-create-suon").val();
        paramOrderObj.salad = $("#input-create-salad").val();
        paramOrderObj.loaiPizza = $("#select-loai-pizza option:selected").val();
        paramOrderObj.idVourcher = $("#input-create-id-voucher").val();
        paramOrderObj.idLoaiNuocUong = $("#select-id-loai-nuoc-uong option:selected").val();
        paramOrderObj.soLuongNuoc = $("#input-create-so-luong-nuoc").val();
        paramOrderObj.hoTen = $("#input-create-ho-ten").val();
        paramOrderObj.thanhTien = $("#input-create-thanh-tien").val();
        paramOrderObj.email = $("#input-create-email").val();
        paramOrderObj.soDienThoai = $("#input-create-so-dien-thoai").val();
        paramOrderObj.diaChi = $("#input-create-dia-chi").val();
        paramOrderObj.loiNhan = $("#input-create-loi-nhan").val();
    }

    function callApiToCreateOrder(paramOrderObj) {
        "use strict";
        $.ajax({
            url: "http://42.115.221.44:8080/devcamp-pizza365/orders",
            async: false,
            type: "POST",
            data: JSON.stringify(paramOrderObj),
            contentType: "application/json;charset=UTF-8",
            success: function(responseObject) {
                //debugger;
                gNewOrderObject = responseObject;
                console.log(gNewOrderObject);
                alert("Tạo đơn hàng thành công!!!")
            },
            error: function(error) {
                console.assert(error.responseText);
            }
        });
    }
    
    function resertCreateOrderForm() {
        "use strict";
        $("#input-create-kich-co").val("");
        $("#input-create-duong-kinh").val("");
        $("#input-create-suon").val("");
        $("#input-create-salad").val("");
        $("#select-loai-pizza").val("");
        $("#input-create-id-voucher").val("");
        $("#select-id-loai-nuoc-uong").val("");
        $("#input-create-so-luong-nuoc").val("");
        $("#input-create-ho-ten").val("");
        $("#input-create-thanh-tien").val("");
        $("#input-create-email").val("");
        $("#input-create-so-dien-thoai").val("");
        $("#input-create-dia-chi").val("");
        $("#input-create-loi-nhan").val("");
    }

    //hàm validate dữ liệu
    function validateData(paramOrderObj) {
        "use strict";
        if (paramOrderObj.kichCo == "") {
            alert("Bạn cần chọn kích cỡ Pizza!!!")
            return false;
        }
        if (paramOrderObj.loaiPizza == "") {
            alert("Bạn cần chọn Loại Pizza!!!")
            return false;
        }
        if (paramOrderObj.idLoaiNuocUong == "") {
            alert("Bạn cần chọn Loại nước uống!!!")
            return false;
        }
        if (paramOrderObj.hoTen == "") {
            alert("Bạn cần nhập họ tên!!!")
            return false;
        }
        if (paramOrderObj.email.length > 0) {
            var vCheckEmail = validateEmail(paramOrderObj.email);
            if (vCheckEmail === false) {
                return false;
            }
        }
        if (paramOrderObj.soDienThoai == "") {
            alert("Bạn cần nhập số điện thoại!!!")
            return false;
        }
        if (paramOrderObj.diaChi == "") {
            alert("Bạn cần nhập địa chỉ!!!")
            return false;
        }

        return true;
    }

    function validateEmail(paramEmail) {
        "use strict";
        var mailformat = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        var vCheckEmail = true;
        if (!mailformat.test(paramEmail)) {
            alert("Email chưa hợp lệ");
            vCheckEmail = false;
        }
        return vCheckEmail;
    }

    function callApiToUpdateOrder(paramId, paramUpdateOrder){
        "use strict";
        const vBASE_URL = "http://42.115.221.44:8080/devcamp-pizza365/orders";
        $.ajax({
          url: vBASE_URL + "/" + paramId,
          async: false,
          type: "PUT",
          data: JSON.stringify(paramUpdateOrder),
          contentType: "application/json;charset=UTF-8",
          success: function(responseObject){
            //debugger;
            console.log(responseObject);
          },
            error: (error) => {
                console.assert(error.responseText);
            }
      })
    }

    // load data to table
    function loadDataToTable(paramResponseObject) {
        //Xóa toàn bộ dữ liệu đang có của bảng
        gOrderTable.clear();
        //Cập nhật data cho bảng 
        gOrderTable.rows.add(paramResponseObject);
        //Cập nhật lại giao diện hiển thị bảng
        gOrderTable.draw();
    }

    function loadDataToModal(gOrderObjectId) {
        "use strict";
        $("#input-update-kich-co").val(gOrderObjectId.kichCo);
        $("#input-update-duong-kinh").val(gOrderObjectId.duongKinh);
        $("#input-update-suon").val(gOrderObjectId.suon);
        $("#input-update-salad").val(gOrderObjectId.salad);

        $("#input-update-so-luong-nuoc").val(gOrderObjectId.soLuongNuoc);
        $("#input-update-thanh-tien").val(gOrderObjectId.thanhTien);

        $("#select-update-loai-pizza").val(gOrderObjectId.loaiPizza);
        $("#input-update-id-voucher").val(gOrderObjectId.idVourcher);
        $("#select-update-id-loai-nuoc-uong").val(gOrderObjectId.idLoaiNuocUong);
        $("#input-update-so-luong-nuoc").val(gOrderObjectId.soLuongNuoc);
        $("#input-update-ho-ten").val(gOrderObjectId.hoTen);
        $("#input-update-email").val(gOrderObjectId.email);
        $("#input-update-so-dien-thoai").val(gOrderObjectId.soDienThoai);
        $("#input-update-dia-chi").val(gOrderObjectId.diaChi);
        $("#input-update-loi-nhan").val(gOrderObjectId.loiNhan);
        $("#input-update-trang-thai").val(gOrderObjectId.trangThai);     
    }   

    function getDrinkList() {
        "use strict";
        $.ajax({
            url: "http://42.115.221.44:8080/devcamp-pizza365/drinks",
            type: "GET",
            dataType: 'json',
            success: function(responseObject) {
                //debugger;
                console.log(responseObject);
                loadDrinkToModal(responseObject);
            },
            error: function(error) {
                console.assert(error.responseText);
            }
        })
    }

    function onBtnModalDeleteConfirmOrderClick(){
        "use strict";
        console.log('Nút confirm delete trên modal delete được ấn !!!')
        callApiDeleteOrder(gDataRow.id);
        callApiToGetAllOrder();
        loadDataToTable(gOrderObject);
        $("#delete-confirm-modal").modal("hide");
    }

    function callApiDeleteOrder(paramId) {
        "use strict";
        const gBASE_URL = "http://42.115.221.44:8080/devcamp-pizza365/orders";
        $.ajax({
            url: gBASE_URL + "/" + paramId,
            type: "DELETE",
            data: 'json',
            async: false,
            success: function(responseObject) {
                console.log(responseObject);
                alert('Xoá đơn hàng ' + gDataRow.orderId + ' thành công !!!');
            },
            error: (error) => {
                console.assert(error.responseText);
            }
        })
    }

    function loadDrinkToModal(paramDrinkObject) {
        "use strict";
        $.each(paramDrinkObject, function(i, item) {
            $("#select-id-loai-nuoc-uong").append($('<option>', {
                text: item.tenNuocUong,
                value: item.maNuocUong
            }))
        })
    }

    
})